package oops_Concept;

public class MainClass {

	public static void main(String[] args) {

		ClassOne a = new ClassOne();

		a.display();

		ClassTwo b = new ClassTwo();

		b.display();

		System.out.println(b.add(4, 2));

		System.out.println(b.add(5., 2.)); // polymorphism

		EncapsulationTest encap = new EncapsulationTest();

		encap.setName("Reshma ");

		System.out.print(encap.getName());

		TwoWheeler test = new Honda();

		test.run();

	}

}
