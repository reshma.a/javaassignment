package oops_Concept;

public class ClassTwo extends ClassOne {

	// v@Override

	public void display() {

		System.out.println("class Two");

	}

	public int add(int x, int y) {

		return x + y;

	}

	// Overload

	public double add(double x, double y) {

		return x + y;

	}

}
