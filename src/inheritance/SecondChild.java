package inheritance;

public class SecondChild extends Parent{
	public void property3() {
		System.out.println("Property 3");
	}
	
	public static void main(String[] args) {
		Parent p = new Parent();
		p.property1();    //Parent property
		//p.property2();  //Child1 property
		//p.property3();  //Child2 property
		
		FirstChild c1 = new FirstChild();
		c1.property1();    //Parent property
		c1.property2();    //Child1 property
		//c1.property3();  //Child2 property
		
		
		SecondChild c2 = new SecondChild();
		c2.property1();  //Parent property
		c2.property3();  //Child1 property
	//	c2.property2();  ////Child2 sproperty
	}

}
