package inheritance;

public class Child extends Parent {
	public void property3() {
		System.out.println("Property 3");
	}
	
	public static void main(String[] args) {
		
		Child c = new Child();
		c.property1();            
		c.property1();
		c.property3();
		
		GrandParent g = new GrandParent();
		g.property1();         
		
		Parent p = new Parent();
		
		p.property1();       
		
		
		
		
	}

}
