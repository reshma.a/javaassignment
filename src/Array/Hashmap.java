package Array;

import java.util.HashMap;



public class Hashmap {

	public static void main(String[] args) {
		HashMap <Integer,String> map= new HashMap<Integer, String>();
		
		//Add element
		map.put(1, "a");
		map.put(2, "b");
		map.put(3, "c");
		map.put(4, "d");
		System.out.println(map);
		
		//Remove element on key based
		map.remove(2);
		System.out.println("After remove value of 2nd key:-"+map);
		
		//
		map.putIfAbsent(2, "b");
		System.out.println("After  adding value at 2nd key:-"+map);
		System.out.println(map);
	
		map.replace(4, "M");
		System.out.println("After replacing value of 4th key:-"+map);
		
		
		
		
	}
}

