package Array;

import java.util.TreeSet;

public class Treeset {
	
	public static void main(String[] args) {
		
		//  TreeSet<String>  al=new TreeSet<String>();  
		TreeSet<String> set = new TreeSet<String>();
		
		set.add("A");
		set.add("B");
		set.add("c");
		set.add("c");
		set.add("D");
		set.add("E");
		set.add("F");
		
		
		System.out.println(set);
		
		 //poolFirst
		 Object p = set.pollFirst();
		 System.out.println(p);
		 
		 //poolLast
		 Object q = set.pollLast();
		 System.out.println(q);
		 
		 System.out.println("Intial :"+set);
		 
		 //headSet
		 System.out.println("Head : " +set.headSet("A"));
		 
		 //tailSet
		 System.out.println("Tail: "+set.tailSet("B"));
		 
		 //subSet
		 
		 System.out.println("SubSet: " +set.subSet("C", "D"));
		 
		
		
	}

}
