package Array;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public class arraylist {
	
	public static void main(String[] args) {
		
		//LinkedList list = new LinkedList();
		ArrayList list = new ArrayList();
		
		System.out.println("Before adding :-"+list.size());
		list.add('a');
		list.add('b');
		list.add('c');
		list.add('d');
		list.add('e'); // duplicates allowed in arraylist
		list.add('e');
		list.add(null);  //null allows
	
		
		System.out.println("After adding :-"+list.size());
		
		System.out.println(list);
		
		list.remove(2);
		System.out.println("After removing object at 2nd index:-"+list);
		
		
		//Iterator
		Iterator<String> itr = list.iterator();
		
		while(itr.hasNext()) {
			
			Object itr_value = itr.next();
			System.out.print(itr_value);
			
		}
		
	}
	

}
